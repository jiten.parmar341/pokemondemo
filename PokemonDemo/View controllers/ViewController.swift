//
//  ViewController.swift
//  PokemonDemo
//
//  Created by JK on 26/07/21.
//

import UIKit

class ViewController: UIViewController, APIResponseDelegate
{
    let alertManager = AlertManager()
    
    var apiManager = APIManager()
    
    var srcBar = UISearchBar()
    
    var tblList =  UITableView()
    
    var arrData = [Result]()
    
    var arrFilteredData = [Result]()
    
    var refreshView = UIRefreshControl()
    
    var isFromPaging = false
    
    var offset = Int()
    
    var actIndicator = UIActivityIndicatorView()
    
    var btnSort = UIButton()
    
    //MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        alertManager.controller = self
        
        self.setListner()
        
        self.loadSearchBar()
        
        self.offset = 0
        
        self.loadSortButton()
        
        self.loadTableView()
        
        actIndicator.style = .medium
        
        actIndicator.hidesWhenStopped = true
        
        actIndicator.color = .black
        
        apiManager.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    //MARK: - Custom method
    
    func setListner()
    {
        self.tblList.register(UINib.init(), forCellReuseIdentifier: "CustomCell")
    }
    
    func loadSearchBar()
    {
        srcBar.frame = CGRect.init(x: 0, y: 90, width: self.view.frame.size.width, height: 50)
        srcBar.searchBarStyle = .minimal
        srcBar.delegate = self
        self.view.addSubview(self.srcBar)
    }
    
    func loadTableView()
    {
        tblList.frame = CGRect.init(x: 0, y: self.btnSort.frame.size.height + self.btnSort.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height - 130)
        
        tblList.delegate = self
        tblList.dataSource = self
        
        self.view.addSubview(self.tblList)
        
        self.loadRefreshControl()
        
        self.callAPIToGetData()
    }
    
    func loadRefreshControl()
    {
        refreshView.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tblList.refreshControl = refreshView
    }
    
    func loadSortButton()
    {
        self.btnSort.setTitle("a-z", for: .normal)
        self.btnSort.setTitle("z-a", for: .selected)
        
        self.btnSort.setTitleColor(.black, for: .normal)
        self.btnSort.setTitleColor(.black, for: .selected)
        
        self.btnSort.frame = CGRect.init(x: self.view.frame.size.width - 80, y: self.srcBar.frame.size.height + srcBar.frame.origin.y, width: 80, height: 50)
        
        self.btnSort.addTarget(self, action: #selector(btnSortClicked), for: .touchUpInside)
        
        self.view.addSubview(self.btnSort)
    }
    
    @objc func btnSortClicked(sender:UIButton)
    {
        sender.isSelected = !sender.isSelected
        
        loadDataIntoTableView()
    }
    
    @objc func refresh(_ sender: AnyObject)
    {
        self.offset = 0
        self.isFromPaging = false
        self.callAPIToGetData()
    }
    
    func loadDataIntoTableView()
    {
        DispatchQueue.main.async
        {
            self.arrFilteredData = self.arrData.filter({ (objResult) -> Bool in
                if self.srcBar.text!.isEmpty
                {
                    return true
                }
                
                return objResult.name.contains(self.srcBar.text!.lowercased())
            })
            
            if self.btnSort.isSelected
            {
                self.arrFilteredData = self.arrFilteredData.sorted(by: {$0.name > $1.name})
            }
            else
            {
                self.arrFilteredData = self.arrFilteredData.sorted(by: {$0.name < $1.name})
            }
            
            self.refreshView.endRefreshing()
            self.actIndicator.stopAnimating()
            self.tblList.reloadData()
        }
    }
    
    //MARK: - Call api to get data
    
    func callAPIToGetData()
    {
        apiManager.loadDataFromAPIForMainScreen(offset: self.offset)
    }
    
    //MARK: - API Response Delegate
    
    func returnResponse(result: PokemonModel)
    {
        if let results = result.results
        {
            if !self.isFromPaging
            {
                self.arrData = results
            }
            else
            {
                self.arrData = self.arrData + results
            }
        }
        
        self.isFromPaging = false
        
        self.loadDataIntoTableView()
    }
    
    func returnResponseDetail(result: DetailData) {
        
    }
    
    func returnResponseWithError(errorString: String)
    {
        self.alertManager.showAlert()
    }
}

extension ViewController : UIScrollViewDelegate
{
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        srcBar.resignFirstResponder()
        
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isFromPaging)
        {
            self.isFromPaging = true
            self.tblList.reloadData()
            
            self.offset = self.offset + 20
            
            self.callAPIToGetData()
        }
    }
}

extension ViewController : UISearchBarDelegate
{
    //MARK: - Search bar delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        srcBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        self.loadDataIntoTableView()
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource
{
    //MARK: - Tableview delegate and datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrFilteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = self.arrFilteredData[indexPath.row].name
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        if isFromPaging
        {
            let bottomRefreshView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
                        
            actIndicator.center = CGPoint.init(x: bottomRefreshView.frame.size.width/2, y: 10)
            
            actIndicator.startAnimating()
            
            bottomRefreshView.addSubview(actIndicator)
            
            return bottomRefreshView
        }
        
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if isFromPaging
        {
            return 50
        }
        
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let objDetail = DetailVC()
        objDetail.view.backgroundColor = .white
        objDetail.urlToLoad = self.arrFilteredData[indexPath.row].url
        self.navigationController?.pushViewController(objDetail, animated: true)
    }
}

