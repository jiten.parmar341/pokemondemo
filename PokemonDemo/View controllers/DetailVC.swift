//
//  DetailView.swift
//  PokemonDemo
//
//  Created by JK on 29/07/21.
//

import UIKit

class DetailVC: UIViewController, APIResponseDelegate
{
    var tblAbility =  UITableView()
    var alertManager = AlertManager()
    var arrAbility = [Abilities]()
    var apiManager = APIManager()
    var urlToLoad = String()
    
    //MARK: - View life cycle
    
    override func viewDidLoad()
    {
        apiManager.delegate = self
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.loadTableView()
    }
    
    //MARK: - Custom method
    
    func loadTableView()
    {
        tblAbility.frame = CGRect.init(x: 0, y: 50 , width: self.view.frame.size.width, height: self.view.frame.size.height - 130)
        
        tblAbility.delegate = self
        tblAbility.dataSource = self
        
        self.view.addSubview(self.tblAbility)
        
        self.apiManager.loadDetailData(url: urlToLoad)
    }
    
    //MARK: - API Response delegate
    
    func returnResponse(result: PokemonModel) {
        
    }
    
    
    func returnResponseDetail(result: DetailData)
    {
        if let foundAbility = result.abilities
        {
            self.arrAbility = foundAbility
        }
        
        self.tblAbility.reloadData()
    }   
    
    func returnResponseWithError(errorString: String)
    {
        self.alertManager.showAlert()
    }
}

extension DetailVC : UITableViewDelegate, UITableViewDataSource
{
    //MARK: - Tableview delegate and datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrAbility.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = self.arrAbility[indexPath.row].ability?.name
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0.01
    }
}
