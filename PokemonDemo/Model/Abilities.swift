//
//  Abilities.swift
//  PokemonDemo
//
//  Created by jiten parmar on 29/07/21.
//

import Foundation

struct Abilities: Decodable
{
    var ability:Result? = nil
    var isHidden:Bool? = nil
    var slot:Int? = nil
}
