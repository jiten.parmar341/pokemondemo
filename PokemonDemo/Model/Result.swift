//
//  result.swift
//  PokemonDemo
//
//  Created by JK on 28/07/21.
//

import UIKit

struct Result: Decodable
{
    var name = String()
    var url = String()
}
