//
//  PokemonModel.swift
//  PokemonDemo
//
//  Created by JK on 28/07/21.
//

import UIKit

struct PokemonModel: Decodable
{
    var count:Int? = nil
    var next:String? = nil
    var previous:String? = nil
    var results:[Result]? = nil
}
