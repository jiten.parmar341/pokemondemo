//
//  AppDelegate.swift
//  PokemonDemo
//
//  Created by JK on 26/07/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        // Override point for customization after application launch.
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let vc = ViewController()
        vc.view.backgroundColor = .white
        
        let navController = UINavigationController.init(rootViewController: vc)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        
        return true
    }
}

