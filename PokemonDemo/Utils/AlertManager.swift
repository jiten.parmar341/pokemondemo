//
//  AlertManager.swift
//  PokemonDemo
//
//  Created by JK on 29/07/21.
//

import UIKit

class AlertManager: NSObject
{
    var controller = UIViewController()
    
    func showAlert()
    {
        let alertController = UIAlertController.init(title: "PokemonDemo", message: "Something went wrong, please try again later", preferredStyle: .alert)
        
        let alertAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        
        alertController.addAction(alertAction)
        
        DispatchQueue.main.async
        {
            self.controller.present(alertController, animated: true, completion: nil)
        }
    }
}
