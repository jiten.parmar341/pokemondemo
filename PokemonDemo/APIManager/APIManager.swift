//
//  APIManager.swift
//  PokemonDemo
//
//  Created by JK on 29/07/21.
//

import UIKit

protocol APIResponseDelegate
{
    func returnResponse(result:PokemonModel)
    func returnResponseDetail(result:DetailData)
    func returnResponseWithError(errorString:String)
}

class APIManager : NSObject
{
    var delegate : APIResponseDelegate?
    
    func loadDataFromAPIForMainScreen(offset:Int)
    {
        let url = "https://pokeapi.co/api/v2/pokemon/?offset=\(offset)&limit=20"
        
        URLSession.shared.dataTask(with: NSURL(string: url)! as URL) { data, response, error in
            // Handle result
            
            if error != nil
            {
                self.delegate?.returnResponseWithError(errorString: error!.localizedDescription)
            }
            else
            {
                do
                {
                    let jsonDecoder = JSONDecoder()
                    let modelData = try jsonDecoder.decode(PokemonModel.self, from: data!)
                    
                    self.delegate?.returnResponse(result: modelData)
                }
                catch
                {
                    print(error)
                }
            }
            
        }.resume()
    }
    
    func loadDetailData(url:String)
    {       
        URLSession.shared.dataTask(with: NSURL(string: url)! as URL) { data, response, error in
            // Handle result
            
            if error != nil
            {
                self.delegate?.returnResponseWithError(errorString: error!.localizedDescription)
            }
            else
            {
                do
                {
                    let jsonDecoder = JSONDecoder()
                    let modelData = try jsonDecoder.decode(DetailData.self, from: data!)
                    
                    self.delegate?.returnResponseDetail(result: modelData)
                }
                catch
                {
                    print(error)
                }
            }
            
        }.resume()
    }
}
